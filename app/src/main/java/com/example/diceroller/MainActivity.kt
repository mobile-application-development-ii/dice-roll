package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

/**
 * This acitivity allows the user to roll a dice and view result
 * on the screen
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
//            val toast = Toast.makeText(this,"Dice Rolled!", Toast.LENGTH_SHORT)
//            toast.show()
            rollDice()
        }
    }

    /**
     * Roll the dice and update screen with the result
     */

    private fun rollDice() {
        // Create new Dice object with 6 sides and roll it
        val dice = Dice(6)
        val diceRoll = dice.roll()
        // Update the screen with the dice rolled result
        val diceImage: ImageView = findViewById(R.id.imageView)
        val drawableResource = when(diceRoll){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        diceImage.setImageResource(drawableResource)
        diceImage.contentDescription = diceRoll.toString()




//        if(diceRoll == 1) {
//            diceImage.setImageResource((R.drawable.dice_1))
//        }else if(diceRoll == 2) {
//            diceImage.setImageResource((R.drawable.dice_2))
//        }else if(diceRoll == 3) {
//            diceImage.setImageResource((R.drawable.dice_3))
//        }else if(diceRoll == 4) {
//            diceImage.setImageResource((R.drawable.dice_4))
//        }else if(diceRoll == 5) {
//            diceImage.setImageResource((R.drawable.dice_5))
//        }else if(diceRoll == 6) {
//            diceImage.setImageResource((R.drawable.dice_6))
//        }

    }
}